#!/usr/bin/env bash
set -e
CPATH=$(osascript -e 'tell application "Finder" to POSIX path of ((folder of window 1) as alias)');
cd $CPATH;
EPATH="$(osascript -e 'tell application "Finder" to get the name of the front window')";
CDATE="$(date +%m%d%y)";
NPATH="${CPATH}/${EPATH}_${CDATE}"
[[ ! -d "${NPATH}" ]] && mkdir -p "${NPATH}" && exit;
